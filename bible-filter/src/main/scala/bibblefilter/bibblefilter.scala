package bibblefilter

// book|chapter#|verse#|verse_text

object ChapterFilter {
  def filter(chapNum: Int) =
    (line: String) => line.split('|')(1).toInt == chapNum
}
