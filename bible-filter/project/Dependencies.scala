import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"

  val sparkVersion = "2.2.2"
  lazy val scalaSpark = Seq(
    "org.apache.spark" %% "spark-core" % sparkVersion,
    "org.apache.spark" %% "spark-sql" % sparkVersion,
    "org.apache.spark" %% "spark-mllib" % sparkVersion,
    "org.apache.spark" %% "spark-streaming" % sparkVersion,
    "org.apache.spark" %% "spark-hive" % sparkVersion,
    "mysql" % "mysql-connector-java" % "5.1.16"
  )

  val bibleFilterDeps = Seq(scalaTest % Test) ++ scalaSpark
}
