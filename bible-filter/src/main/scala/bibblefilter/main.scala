package bibblefilter

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext

object BibbleFilterMain extends App {

  if (args.length < 3) {
    println("Usage: inFile chapter outDir")
    sys.exit(1)
  }

  val outputDir = args(2)
  val chapNum = try
    args(1).toInt
  catch {
    case ex: NumberFormatException =>
      println("Chapter Number must be a number!")
      sys.exit(2)

  }

  val spark = SparkSession.builder. // builder pattern to construct the session
    master("local[*]"). // run locally on all cores ([*])
    appName("BibbleFilter").
    config("spark.app.id", "BibbleFilter"). // to silence Metrics warning
    getOrCreate() // create it!

  val sc = spark.sparkContext // get the SparkContext
  // val sqlContext = spark.sqlContext        // get the old SQLContext, if needed
  // import sqlContext.implicits._            // import useful stuff
  // import org.apache.spark.sql.functions._  // here, import min, max, etc.

  val input = sc.textFile(args(0))

  val wordCounts = input.filter(ChapterFilter.filter(chapNum))
                        .flatMap(line => line.split("""[^\p{IsAlphabetic}]+"""))
                        .map(word => (word, 1))
                        .reduceByKey(_ + _)

/*  val book = input.filter(ChapterFilter.filter(chapNum))
  val words = book.flatMap(line => line.split("""[^\p{IsAlphabetic}]+""").map(_.toLowerCase))
  val wordGroups = words.groupBy(w => w)
  val wordCounts = wordGroups.map { case (word, group) => (word, group.size) }*/

  wordCounts.saveAsTextFile(s"$outputDir/kjv-wc-groupby-chapter-$chapNum")

  try {
    sc.stop
  } catch {
    case _ : Throwable => Unit
  }
}
