package wikipedia

import org.apache.spark.{SparkConf, SparkContext, rdd}
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

case class WikipediaArticle(title: String, text: String) {

  /**
    * @return Whether the text of this article mentions `lang` or not
    * @param lang Language to look for (e.g. "Scala")
    */
  def mentionsLanguage(lang: String): Boolean = text.split(' ').contains(lang)
}

object WikipediaRanking {

  val langs = List("JavaScript",
                   "Java",
                   "PHP",
                   "Python",
                   "C#",
                   "C++",
                   "Ruby",
                   "CSS",
                   "Objective-C",
                   "Perl",
                   "Scala",
                   "Haskell",
                   "MATLAB",
                   "Clojure",
                   "Groovy")

  val spark =
    SparkSession.builder. // builder pattern to construct the session
    master("local[*]"). // run locally on all cores ([*])
    appName("BibbleFilter")
      .config("spark.app.id", "BibbleFilter")
      . // to silence Metrics warning
      getOrCreate()

  val sc = spark.sparkContext

  // Hint: use a combination of `sc.textFile`, `WikipediaData.filePath` and `WikipediaData.parse`
  val wikiRdd: RDD[WikipediaArticle] =
    sc.textFile(WikipediaData.filePath)
      .map(WikipediaData.parse)

  wikiRdd.cache()

  /** Returns the number of articles on which the language `lang` occurs.
    *  Hint1: consider using method `aggregate` on RDD[T].
    *  Hint2: consider using method `mentionsLanguage` on `WikipediaArticle`
    */
  def occurrencesOfLang(lang: String, rdd: RDD[WikipediaArticle]): Int =
    rdd
      .aggregate(0)(
        (acc, article) =>
          acc + {
            if (article.mentionsLanguage(lang))
              1
            else
              0
        },
        _ + _
      )

  // OLD:
  //    wikiRdd
  //      .filter{ _.mentionsLanguage(lang) }
  //      .count()
  //      .toInt

  /* (1) Use `occurrencesOfLang` to compute the ranking of the languages
   *     (`val langs`) by determining the number of Wikipedia articles that
   *     mention each language at least once. Don't forget to sort the
   *     languages by their occurrence, in decreasing order!
   *
   *   Note: this operation is long-running. It can potentially run for
   *   several seconds.
   */
  def rankLangs(langs: List[String],
                rdd: RDD[WikipediaArticle]): List[(String, Int)] = {
    langs
      .map { lang =>
        (lang, occurrencesOfLang(lang, rdd))
      }
      .sortWith((a, b) => a._2 > b._2)
  }

  /* Compute an inverted index of the set of articles, mapping each language
   * to the Wikipedia pages in which it occurs.
   */
  def makeIndex(
      langs: List[String],
      rdd: RDD[WikipediaArticle]): RDD[(String, List[WikipediaArticle])] = {

//    rdd.aggregate(List[(String, WikipediaArticle)]())(
//      (acc, article) => {
//        val mentions = for (l <- langs if (article.mentionsLanguage(l)))
//          yield (l, article)
//
//        acc ++ mentions
//      },
//      (l1, l2) => l1 ++ l2
//    )
    val tuples = rdd
      .flatMap(
        article =>
          for (lang <- langs if article.mentionsLanguage(lang))
            yield (lang, article))

    type ArticleList = List[WikipediaArticle]

    val createCombiner = (article: WikipediaArticle) => List(article)

    val collectArticle =
      (l: ArticleList, article: WikipediaArticle) => article :: l

    val combineLists =
      (l1: ArticleList, l2: ArticleList) => l1 ++ l2

    val merger = (a1: ArticleList, a2: ArticleList) => a1 ++ a2

    val index =
      tuples.combineByKey(createCombiner, collectArticle, combineLists)
    //tuples.groupBy(tup => tup._1)

    index

  }

  /* (2) Compute the language ranking again, but now using the inverted index. Can you notice
   *     a performance improvement?
   *
   *   Note: this operation is long-running. It can potentially run for
   *   several seconds.
   */
  def rankLangsUsingIndex(
      index: RDD[(String, List[WikipediaArticle])]): List[(String, Int)] = {

    index
      .map { case (lang, articles) => (lang, articles.size) }
      .sortBy({ case (lang, occurences) => occurences }, false)
      .collect()
      .toList
  }

  /* (3) Use `reduceByKey` so that the computation of the index and the ranking are combined.
   *     Can you notice an improvement in performance compared to measuring *both* the computation of the index
   *     and the computation of the ranking? If so, can you think of a reason?
   *
   *   Note: this operation is long-running. It can potentially run for
   *   several seconds.
   */
  def rankLangsReduceByKey(langs: List[String],
                           rdd: RDD[WikipediaArticle]): List[(String, Int)] = {

    rdd
      .flatMap(article =>
        for (lang <- langs if article.mentionsLanguage(lang))
          yield (lang, 1))
      .reduceByKey(_ + _)
      .sortBy({ case (k, v) => v }, false)
      .collect()
      .toList

    //langs.map(lang => (article, lang, article.mentionsLanguage(lang))))
  }

  def main(args: Array[String]) {

    /* Languages ranked according to (1) */
    val langsRanked: List[(String, Int)] =
      timed("Part 1: naive ranking", rankLangs(langs, wikiRdd))

    /* An inverted index mapping languages to wikipedia pages on which they appear */
    def index: RDD[(String, List[WikipediaArticle])] =
      makeIndex(langs, wikiRdd)

    /* Languages ranked according to (2), using the inverted index */
    val langsRanked2: List[(String, Int)] =
      timed("Part 2: ranking using inverted index", rankLangsUsingIndex(index))

    /* Languages ranked according to (3) */
    val langsRanked3: List[(String, Int)] = timed(
      "Part 3: ranking using reduceByKey",
      rankLangsReduceByKey(langs, wikiRdd))

    /* Output the speed of each ranking */
    println(timing)
    sc.stop()
  }

  val timing = new StringBuffer
  def timed[T](label: String, code: => T): T = {
    val start = System.currentTimeMillis()
    val result = code
    val stop = System.currentTimeMillis()
    timing.append(s"Processing $label took ${stop - start} ms.\n")
    result
  }
}
